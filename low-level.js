/**
 * The deepest promise in the chain, likely the first to complete.
 * Let see how errors propogate up!
 */

//q for easy async
var Q = require("q");

console.log("low-level.js:  Module started ...");

/**
 * This promise may be rejected or fulfilled.  If fulfilled, a status
 * object will be returned with success, if rejected, an error will
 * be returned.
 *
 * @returns {Q promise} Something that you can wait for to complete
 */
 var mayRejectInPromise = function(){
    console.log("low-level.js: mayRejectInPromise: method started ...");

    //prep for async
    var deferred = Q.defer();

    //define async
    setTimeout(function(){
        if(flipCoin()){
           console.log("low-level.js: mayRejectInPromise: promise Resolved this time!");
           deferred.resolve({success: true, message: "low-level.js: mayRejectInPromise: promise resolved!"});

       } else{
           console.log("low-level.js: mayRejectInPromise: promise Rejected this time!");
           var error = new Error("Error in low-level.js mayRejectInPromise");
           deferred.reject({success: false, message: "low-level.js: mayRejectInPromise: promise rejected with error!", error: error});
       }
   }, 100);
    
    //return async
    return deferred.promise;
}

/**
 * This method may or may not immediately throw an error, if it doesn't
 * a status object will be returned
 *
 * @returns {object} says what happened
 */
 var mayThrowImmediately = function (){
    console.log("low-level.js: mayThrowImmediately: method started ...");
    var status = {};
    if(flipCoin()){
        console.log("low-level.js: mayThrowImmediately: didn't throw an Error this time!");
        status = {success: true, message: "low-level.js: mayThrowImmediately: succeeded immediately"};
    } else{
        console.log("low-level.js: mayThrowImmediately: threw an Error this time!");
        throw new Error("I'm an error THROWN immediately from low-level.js: mayThrowImmediately!"); 
    }
    return status;
}

    /**
 * This method returns a promise that will fulfill with an object
 * like {success: true, message: "it worked"}
 *
 * @returns {Q promise} Something you can wait for to complete
 */
 var fulfillInPromise = function (){
    console.log("low-level.js: fulfillInPromise: method started ...");

    //prep for async
    var deferred = Q.defer();

    //define async
    setTimeout(function(){
        console.log("low-level.js: fulfillInPromise: resolving promise");
        deferred.resolve({success: true, message: "low-level.js: fulfillInPromise: promise fulfilled!"});
    }, 100);
    
    //return async
    return deferred.promise;
}

/**
 * This method returns a promise that will reject with an error!
 *
 * @returns {Q promise} Something you can wait for to complete
 */
 var rejectInPromise = function(){
    console.log("low-level.js: rejectInPromise: method started ...");
    
    //prep for async
    var deferred = Q.defer();

    //define async
    setTimeout(function(){
        console.log("low-level.js: rejectInPromise: rejecting promise with Error");
        deferred.reject(new Error("I'm an error used to reject a promise! from low-level.js: rejectInPromise"));
    }, 100);
    
    //return async
    return deferred.promise;
}

/**
 * This method actually throws an Error in an asynchronous callback
 *
 * @returns {Q promise} Something you can wait for to complete
 */
 var throwInPromise = function (){
    console.log("low-level.js: throwInPromise: method started ...");

    //prep for async
    var deferred = Q.defer();

    //define async
    setTimeout(function(){
        throw new Error("I'm an error THROWN in async callback! from low-level.js: throwInPromise");
        console.log("low-level.js: throwInPromise: error thrown!");
        console.log("low-level.js: throwInPromise: resolving");

        //YOU WOULD NEVER DO THIS, I'm banking on it not getting here
        deferred.resolve("We threw the error");
    }, 100);    
    
    //return async
    return deferred.promise;
}

/**
 * This method does not async, it just throws an Error
 */
 var throwImmediately = function (){
    console.log("low-level.js: throwImmediately: method started ...");
    throw new Error("I'm an error THROWN immediately from low-level.js: throwImmediately!"); 
}

/**
 * This method successfully catches an immediately thrown error
 *
 * @returns {object} says what happened
 */
 var catchImmediately = function(){
    console.log("low-level.js: catchImmediately: method started ...");
    var status = {};
    try{
        throw new Error("I'm an error CAUGHT immediately from low-level.js: catchImmediately!");
    } catch (error){
        status.success = false;
        status.error = error;
        status.message = "An Error was caught in low-level.js: catchImmediately!";
    }

    return status;
}

/**
 * An easy non-async success case
 *
 * @returns {object} saying what happened
 */
 var dontThrowImmediately = function(){
    return {success: true, message: "low-level.js: dontThrowImmediately: succeeded immediately"};
}

/**
 * Returns either 0 or 1, basically a coin toss
 *
 * @returns {int} 0 or 1
 */
 var flipCoin = function(){
    return Math.round(Math.random());
}


module.exports = {
    mayRejectInPromise: mayRejectInPromise,
    mayThrowImmediately: mayThrowImmediately,
    fulfillInPromise: fulfillInPromise,
    rejectInPromise: rejectInPromise,
    throwInPromise: throwInPromise,
    throwImmediately: throwImmediately,
    dontThrowImmediately: dontThrowImmediately
}