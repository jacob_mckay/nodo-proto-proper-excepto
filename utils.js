/**
 * Small utility module to transform objects <==> xml
 */
var xml2js = require('xml2js');

var objectToXML = function (object) {
    var builder = new xml2js.Builder();
    var xml = builder.buildObject(object);
    return cleanXmlString(xml);
};

var cleanXmlString = function (xml) {
    xml = xml.replace(/\r?\n|\r/g, "");
    xml = xml.replace(/>\s*/g, '>');
    xml = xml.replace(/\s*</g, '<');
    return xml;
};

var xmlToObject = function (xml) {
    console.log("Converting XML to object");
    var deferred = Q.defer();
    xml2js.parseString(xml, {trim:true}, function(err, result){
        if ( err ){
            console.log("Converting XML to object ... ERROR [" + err.toString() + "]");
            deferred.resolve(null);
            return;
        }
        console.log("Converting XML to object ... SUCCESS");
        if ( !result ){
            logger.debug("Result is null");
        }
        else{
        	console.log("Result is not null");
        }
        deferred.resolve(result);

    });
    return deferred.promise;
};

module.exports = {
    objectToXML: objectToXML,
    xmlToObject: xmlToObject
}