/**
 * This program will be called first in this prototype exception exerciser thingy.  
 * It is the first to be called of three files (the other two are med-level and 
 * low-level).  Executing them in a chain (of promises) will allow us to demonstrate
 * how to handle multiple scenarios of when/where exceptions happend and how to handle
 * them.  This is a commandline App
 */

//includes
var ml = require("./med-level.js"); //module used for demonstration
var Q = require("q"); //used for easy async

console.log("high-level.js:  Module started ...");


/**
 * Demonstrates how to successfully handle non-error cases as well
 * as propogating errors
 */
 var handleEverythingProperly = function(){
 	console.log("high-level.js: handleEverythingProperly: Showing you how it's done THE COMPLETE WAY");
 	console.log("high-level.js: handleEverythingProperly: This is how you should do it!");

 	//start off only one layer deep
 	console.log("high-level.js: handleEverythingProperly: Testing one layer deep!");
 	handleEverythingProperlyOneLayer();

 	//start off only one layer deep
 	console.log("high-level.js: handleEverythingProperly: Testing two layers deep!");
 	handleEverythingProperlyTwoLayers();

 	console.log("high-level.js: handleEverythingProperly: Showing you how it's done THE COMPLETE WAY - DONE!!");

 }

/**
 * Only demonstrates how to handle errors (exceptions, rejections)
 */
 var handleOnlyErrors = function(){
 	console.log("high-level.js: handleOnlyErrors: Showing you how to simply handle errors...");

 	//start off only one layer deep (just using med-level)
 	console.log("high-level.js: handleOnlyErrors: Testing one layer deep");
 	handleOnlyErrorsOneLayer();

 	//now go two layers deep (invoke low-level from med-level)
 	console.log("high-level.js: handleOnlyErrors: Testing two layers deep now");
 	handleOnlyErrorsTwoLayers();

 	console.log("high-level.js: handleOnlyErrors: Showing you how to simply handle errors DONE!!!");
 }

/**
 * Only demonstrates how to handle successful successful calls
 * (fulfillments)
 */
 var handleOnlySuccesses = function(){
 	console.log("high-level.js: handleOnlySuccesses: Showing you how to not handle errors (by only handling successes)...");

 	//start off only one layer deep (just using med-level)
 	console.log("high-level.js: handleOnlySuccesses: Testing one layer deep");
 	handleOnlySuccessesOneLayer();

 	//now go two layers deep (invoke low-level from med-level)
 	console.log("high-level.js: handleOnlySuccesses: Testing two layers deep now");
 	handleOnlySuccessesTwoLayers();

 	console.log("high-level.js: handleOnlySuccesses: Showing you how to not handle errors (by only handling successes) DONE!!!");

 }

 var handleEverythingProperlyOneLayer = function(){
	//sync/immediate succesess first
	console.log("high-level.js: handleEverythingProperly: Testing out immediate (sync) errors...");
	console.log("high-level.js: handleEverythingProperly: Testing out mayThrowImmediately");

	//use try/catch to handle synchronous errors
	try{
		var result = ml.mayThrowImmediately();
		console.log("high-level.js: handleEverythingProperly: mayThrowImmediately did not thow error");
		console.log("high-level.js: handleEverythingProperly: mayThrowImmediately response was: ", result);
	}catch(error){
		console.log("high-level.js: handleEverythingProperly: mayThrowImmediately threw error: ", error);
	}

	//use promise().then(fulfillment handler).fail(error handler) to account for errors in promises/callbacks
	console.log("high-level.js: handleEverythingProperly: Testing out mayRejectInPromise...");
	ml.mayRejectInPromise().then(function(result){
		console.log("high-level.js: handleEverythingProperly: mayRejectInPromise did not reject!");
		console.log("high-level.js: handleEverythingProperly: mayRejectInPromise response: ", result);
	}).fail(function(error){
		console.log("high-level.js: handleEverythingProperly: mayRejectInPromise was rejected");
		console.log("high-level.js: handleEverythingProperly: mayRejectInPromise error was: ", error);
	});
}

var handleEverythingProperlyTwoLayers = function(){
//sync/immediate succesess first
console.log("high-level.js: handleEverythingProperly: Testing out immediate (sync) errors...");
console.log("high-level.js: handleEverythingProperly: Testing out useLLmayThrowImmediately");

	//if low-level handled it correctly as is best practice we should be good
	var result = ml.useLLmayThrowImmediately();
	console.log("high-level.js: handleEverythingProperly: lower-level should've isolated the error (don't propogate synchronous errors, unless appropriate)");
	console.log("high-level.js: handleEverythingProperly: useLLmayThrowImmediately response was: ", result);
	
	//use promise().then(fulfillment handler).fail(error handler) to account for errors in promises/callbacks
	console.log("high-level.js: handleEverythingProperly: Testing out useLLmayRejectInPromise...");
	ml.useLLmayRejectInPromise().then(function(result){
		console.log("high-level.js: handleEverythingProperly: useLLmayRejectInPromise did not reject!");
		console.log("high-level.js: handleEverythingProperly: useLLmayRejectInPromise response: ", result);
	}).fail(function(error){
		console.log("high-level.js: handleEverythingProperly: useLLmayRejectInPromise was rejected");
		console.log("high-level.js: handleEverythingProperly: useLLmayRejectInPromise error was: ", error);
	});
}

var handleOnlySuccessesOneLayer = function(){
 	//sync/immediate succesess first
 	console.log("high-level.js: handleOnlySuccesses: Testing out immediate (sync) errors...");
 	console.log("high-level.js: handleOnlySuccesses: Testing out dontThrowImmediately");

 	var result = ml.dontThrowImmediately();
 	console.log("high-level.js: handleOnlySuccesses: response from dontThrowImmediately: ", result);

 	//async fulfill promise 
 	console.log("high-level.js: handleOnlySuccesses: Testing out fulfillInPromise...");
 	ml.fulfillInPromise().then(function(result){
 		console.log("high-level.js: handleOnlySuccesses: fulfillInPromise response: ", result);
 	});
 }

 var handleOnlySuccessesTwoLayers = function(){
 	//sync/immediate succesess first
 	console.log("high-level.js: handleOnlySuccesses: Testing out immediate (sync) errors...");
 	console.log("high-level.js: handleOnlySuccesses: Testing out useLLdontThrowImmediately");

 	var result = ml.useLLdontThrowImmediately();
 	console.log("high-level.js: handleOnlySuccesses: response from useLLdontThrowImmediately: ", result);

 	//async fulfill promise 
 	console.log("high-level.js: handleOnlySuccesses: Testing out useLLfulfillInPromise...");
 	ml.useLLfulfillInPromise().then(function(result){
 		console.log("high-level.js: handleOnlySuccesses: useLLfulfillInPromise response: ", result);
 	});
 }


 var handleOnlyErrorsOneLayer = function(){
 	//sync/immediate errors first
 	//non-sync immediate errror
 	console.log("high-level.js: handleOnlyErrors: Testing out immediate (sync) errors...");
 	console.log("high-level.js: handleOnlyErrors: Testing out throwImmediately");
 	try{
 		ml.throwImmediately();
 	}catch(error){
 		console.log("high-level.js: handleOnlyErrors: throwImmediately error: ", error);
 	}

 	// //sync error thrown in promise
 	// console.log("high-level.js: handleOnlyErrors: Testing out throwInPromise...");
 	// ml.throwInPromise().fail(function(error){
 	// 	console.log("high-level.js: handleOnlyErrors: throwInPromise response: ", error.toString());
 	// });

 	//async reject promise with error
 	console.log("high-level.js: handleOnlyErrors: Testing out rejectInPromise...");
 	ml.rejectInPromise().fail(function(error){
 		console.log("high-level.js: handleOnlyErrors: rejectInPromise response: ", error);
 	});
 }

 var handleOnlyErrorsTwoLayers = function(){
 	//sync/immediate errors first
 	//non-sync immediate errror
 	console.log("high-level.js: handleOnlyErrors: Testing out immediate (sync) errors...");
 	console.log("high-level.js: handleOnlyErrors: Testing out useLLthrowImmediately");
 	var response = ml.useLLthrowImmediately();
 	console.log("high-level.js: handleOnlyErrors: useLLthrowImmediately response: ", response);

 	// //sync error thrown in promise
 	// console.log("high-level.js: handleOnlyErrors: Testing out useLLthrowInPromise...");
 	// ml.useLLthrowInPromise().fail(function(error){
 	// 	console.log("high-level.js: handleOnlyErrors: useLLthrowInPromise response: ", error.toString());
 	// });

 	//async reject promise with error
 	console.log("high-level.js: handleOnlyErrors: Testing out useLLrejectInPromise...");
 	ml.useLLrejectInPromise().fail(function(error){
 		console.log("high-level.js: handleOnlyErrors: useLLrejectInPromise response: ", error);
 	});
 }

 module.exports = {
 	handleEverythingProperly: handleEverythingProperly,
 	handleOnlySuccesses: handleOnlySuccesses,
 	handleOnlyErrors: handleOnlyErrors
 }
