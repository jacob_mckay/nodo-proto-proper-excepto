Wassap y'all!  Lets make sure we are handling exceptions succintly and completely so we know our services are ROCK SOLID!  Let your lava flow!!"


We're finna figure out exceptions and handling them so our code is succint, complete, + resilient
 We're using the Q library: [https://github.com/kriskowal/q] to handle promises (an async mechanism) for us
 This little program will demonstrate proper exception handling when knee-deep in promise chains.
 It draws heavily from the docs: [http://documentup.com/kriskowal/q/]
 And the method-by-method API reference: [https://github.com/kriskowal/q/wiki/API-Reference]
 Here's Joyents docs on error handling: [https://www.joyent.com/developers/node/design/errors] which I found to be a truly informing read.
 More knowledge: [http://strongloop.com/strongblog/robust-node-applications-error-handling/]  comes with a checklist:

 Errors happen; its important to build structures to handle them. Use this checklist to buff up your code:

*Where am I using throw? Am I prepared to handle these explicit exceptions when they occur?
*Am I safeguarding against common sources for implicit exceptions (like JSON.parse, undefined data values in a nodeback)?
*Am I handling ‘error’ events on all EventEmitters?
*Am I handling all error arguments in nodebacks?
*Am I notified of uncaught exceptions?

Other useful material might be found regarding event emitters [http://nodejs.org/api/events.html] which we could extend for long running operations/connections that have varied levels of progress we want to keep track of.
Lastly, unexpected errors can be capture with Domains [http://nodejs.org/docs/latest/api/domain.html] and process.on('uncaughtException'), but in my readings I've found that to be bad practice.

For fun/understanding, comment out the "throwInPromise" methods in high-level.js.  You will see that without alternative methods (using domains or overriding the process error handler, both of which are bad practice) the Errors get thrown when they async function finishes and you cannot catch it with standard promise functions .then, .done, .catch, .fail, etc.  I think it is instructive that the error occurrs after the rest of the program has executed, illustrating the need to handle errors where they will occur.