//Driver for this learning exercise, uses three files to test out
//the propogation and proper handling of errors.  Checkout the
//README for more info

//includes
//high-level file puts the lower-level modules through their paces
var hl = require("./high-level.js");

//handle arguements
 /** 
 * The one and only arguement to this program, can be: 
 * "successes-only" - only illustrates how to just handle things going well
 * "errors-only" - only illustrates how to just handle things going wrong
 * "proper" - demonstrates how to properly handle things going well and wrong!
 * 
 * @type {String} 
 */
 var mode = process.argv[2]; 

 
 //default mode to "errors-only"
 mode = !mode ? "proper" : mode;
 console.log("Running high-level.js using mode: ", mode);

  //Begin main program
  switch(mode){

    //this just shows how to handle various errors for demonstrative
    //purposes
  	case "errors-only":
  	hl.handleOnlyErrors();
  	break;

    //this shows no error handling (and doesn't throw any either,
    //it is essentially a naive programmers dream-land)
  	case "successes-only":
  	hl.handleOnlySuccesses();
  	break;

    //this shows how to handle both cases across various errors that 
    //might occur
  	case "proper":
  	default:
  	hl.handleEverythingProperly();
  }