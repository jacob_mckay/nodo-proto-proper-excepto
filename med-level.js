/**
 * The second most deep promise in the chain, likely the seconed
 * complete.
 * Let see how errors propogate up!
 */

//q for easy async
var Q = require("q");
//low-level file that we will be using
var ll = require("./low-level.js");

console.log("med-level.js:  Module started ...");

/**
 * This promise may be rejected or fulfilled.  If fulfilled, a status
 * object will be returned with success, if rejected, an error will
 * be returned.
 *
 * @returns {Q promise} Something that you can wait for to complete
 */
 var useLLmayRejectInPromise = function(){
 	console.log("med-level.js: useLLmayRejectInPromise: method started ...");

    //prep for async
    var deferred = Q.defer();

    ll.mayRejectInPromise().then(function(response){
    	//in the event of fulfillment this code will run
    	console.log("med-level.js: useLLmayRejectInPromise: llmayRejectInPromise resolved, and so will we");
    	deferred.resolve({success: true, message: "llmayRejectInPromise had no error and fulfilled"});
    }).fail(function(error){
    	//in the event of rejection this code will run
    	console.log("med-level.js: useLLmayRejectInPromise: llmayRejectInPromise rejected with error, and so will we (propogate)");
    	console.log("med-level.js: useLLmayRejectInPromise: we should also consider extending the error with more information");
    	deferred.reject({success: false, message: "med-level.js: usellmayRejectInPromisewas rejected and "})
    	deferred.reject
    });

    //return async
    return deferred.promise;
}

/**
 * This method may or may not immediately throw an error, if it doesn't
 * a status object will be returned
 *
 * @returns {object} says what happened
 */
 var useLLmayThrowImmediately = function (){
 	console.log("med-level.js: useLLmayThrowImmediately: method started ...");
 	var status = {};
 	try{
 		console.log("med-level.js: useLLmayThrowImmediately: llmayThrowImmediately did not throw this time!");
 		status = ll.mayThrowImmediately();
 		status.message = "useLLmayThrowImmediately succeeded without catching an error in llmayThrowImmediately";
 	}catch(error){
 		console.log("med-level.js: useLLmayThrowImmediately: llmayThrowImmediately did throw an error this time: ", error);
 		status = {success: false, message: "llmayThrowImmediately: did throw this time", error: error};
 	}
 	return status;
 }

/**
 * This method returns a promise that will fulfill with an object
 * like {success: true, message: "it worked"}
 *
 * @returns {Q promise} Something you can wait for to complete
 */
 var useLLfulfillInPromise = function (){
 	console.log("med-level.js: useLLfulfillInPromise: method started ...");

    //prep for async
    var deferred = Q.defer();

    //define async
    ll.fulfillInPromise().then(function(result){
    	console.log("med-level.js: useLLfulfillInPromise: result from low-level.js:fulfillInPromise", result);
    	console.log("med-level.js: useLLfulfillInPromise: resolving promise");
    	deferred.resolve({success: true, message: "med-level.js: useLLfulfillInPromise: promise fulfilled!"});
    });
    
    //return async
    return deferred.promise;
}

/**
 * This method returns a promise that will reject with an error!
 *
 * @returns {Q promise} Something you can wait for to complete
 */
 var useLLrejectInPromise = function(){
 	console.log("med-level.js: useLLrejectInPromise: method started ...");

    //prep for async
    var deferred = Q.defer();

    //define async
    ll.rejectInPromise().fail(function(error){
    	console.log("med-level.js: useLLrejectInPromise: result from low-level.js:rejectInPromise: ", error);
    	console.log("med-level.js: useLLrejectInPromise: rejecting promise with Error");
    	deferred.reject(new Error("I'm an error used to reject a promise! from med-level.js: useLLrejectInPromise"));
    });
    
    //return async
    return deferred.promise;
}

/**
 * This method actually throws an Error in an asynchronous callback
 *
 * @returns {Q promise} Something you can wait for to complete
 */
 var useLLthrowInPromise = function (){
 	console.log("med-level.js: useLLthrowInPromise: method started ...");

    //prep for async
    var deferred = Q.defer();

    //define async
    ll.throwInPromise().then(function(result){
    	//ll throw in promise should cause us to never reach this point as this is where
    	//fulfillments are handled
    	console.log("med-level.js: useLLthrowInPromise: in fulfillment handler!  This shouldn't happen");
    	console.log("med-level.js: useLLthrowInPromise: resolving");

    	deferred.resolve({success: true, message: "How did this happen?"});
    });    
    
    //return async
    return deferred.promise;
}

/**
 * This method uses the low-level.js synchronously Error throwing function, and catches 
 * it appropriately
 *
 * @returns {object} What happened.
 */
 var useLLthrowImmediately = function (){
 	console.log("med-level.js: useLLthrowImmediately: method started ...");
 	try{
 		ll.throwImmediately();
 	} catch(error){
 		console.log("med-level.js: useLLthrowImmediately: Error caught in exception form:");
 		console.log(error);
 	}
 	return {success: true, message: "Error in LLthrowImmediately successfully caught"};
 }

/**
 * Using low-level.js's easy synchronous success call
 *
 * @returns {object} says what happened
 */
 var useLLdontThrowImmediately = function(){
 	console.log("med-level.js: useLLdontThrowImmediately: method started ...");
 	var result = ll.dontThrowImmediately();
 	return {success: true, message: "LLdontThrowImmediately successfully called", llResult: result};
 }

/**
 * This promise may be rejected or fulfilled.  If fulfilled, a status
 * object will be returned with success, if rejected, an error will
 * be returned.
 *
 * @returns {Q promise} Something that you can wait for to complete
 */
 var mayRejectInPromise = function(){
 	console.log("med-level.js: mayRejectInPromise: method started ...");

    //prep for async
    var deferred = Q.defer();

    //define async
    setTimeout(function(){
    	if(flipCoin()){
    		console.log("med-level.js: mayRejectInPromise: promise Resolved this time!");
    		deferred.resolve({success: true, message: "med-level.js: mayRejectInPromise: promise resolved!"});

    	} else{
    		console.log("med-level.js: mayRejectInPromise: promise Rejected this time!");
    		var error = new Error("Error in med-level.js mayRejectInPromise");
    		deferred.reject({success: false, message: "med-level.js: mayRejectInPromise: promise rejected with error!", error: error});
    	}
    }, 100);
    
    //return async
    return deferred.promise;
}

/**
 * This method may or may not immediately throw an error, if it doesn't
 * a status object will be returned
 *
 * @returns {object} says what happened
 */
 var mayThrowImmediately = function (){
 	console.log("med-level.js: mayThrowImmediately: method started ...");
 	var status = {};
 	if(flipCoin()){
 		console.log("med-level.js: mayThrowImmediately: didn't throw an Error this time!");
 		status = {success: true, message: "med-level.js: mayThrowImmediately: succeeded immediately"};
 	} else{
 		console.log("med-level.js: mayThrowImmediately: threw an Error this time!");
 		throw new Error("I'm an error THROWN immediately from med-level.js: mayThrowImmediately!"); 
 	}
 	return status;
 }

/**
 * This method returns a promise that will fulfill with an object
 * like {success: true, message: "it worked"}
 *
 * @returns {Q promise} Something you can wait for to complete
 */
 var fulfillInPromise = function (){
 	console.log("med-level.js: fulfillInPromise: method started ...");

    //prep for async
    var deferred = Q.defer();

    //define async
    setTimeout(function(){
    	console.log("med-level.js: fulfillInPromise: resolving promise");
    	deferred.resolve({success: true, message: "med-level.js: fulfillInPromise: promise fulfilled!"});
    }, 100);
    
    //return async
    return deferred.promise;
}

/**
 * This method returns a promise that will reject with an error!
 *
 * @returns {Q promise} Something you can wait for to complete
 */
 var rejectInPromise = function(){
 	console.log("med-level.js: rejectInPromise: method started ...");

    //prep for async
    var deferred = Q.defer();

    //define async
    setTimeout(function(){
    	console.log("med-level.js: rejectInPromise: rejecting promise with Error");
    	deferred.reject(new Error("I'm an error used to reject a promise! from med-level.js: rejectInPromise"));
    }, 100);
    
    //return async
    return deferred.promise;
}

/**
 * This method actually throws an Error in an asynchronous callback
 *
 * @returns {Q promise} Something you can wait for to complete
 */
 var throwInPromise = function (){
 	console.log("med-level.js: throwInPromise: method started ...");

    //prep for async
    var deferred = Q.defer();

    //define async
    setTimeout(function(){
    	throw new Error("I'm an error THROWN in async callback! from med-level.js: throwInPromise");
    	console.log("med-level.js: throwInPromise: error thrown!");
    	console.log("med-level.js: throwInPromise: resolving");

        //YOU WOULD NEVER DO THIS, I'm banking on it not getting here
        deferred.resolve("We threw the error");
    }, 100);    
    
    //return async
    return deferred.promise;
}

/**
 * This method does not async, it just throws an Error
 */
 var throwImmediately = function (){
 	console.log("med-level.js: throwImmediately: method started ...");
 	throw new Error("I'm an error THROWN immediately from med-level.js: throwImmediately!"); 
 	return;
 }

 /**
 * An easy non-async success case
 *
 * @returns {object} saying what happened
 */
 var dontThrowImmediately = function(){
 	return {success: true, message: "med-level.js: dontThrowImmediately: succeeded immediately"};
 }

 /**
 * Returns either 0 or 1, basically a coin toss
 *
 * @returns {int} 0 or 1
 */
 var flipCoin = function(){
    return Math.round(Math.random());
}

/**
 * We export methods that utilize the lowerlevel methods, as well
 * as ones that have the same behavior at this level
 *
 * @type {Object} represents all the functions we export
 */
 module.exports = {
 	useLLfulfillInPromise: useLLfulfillInPromise,
 	useLLrejectInPromise: useLLrejectInPromise,
 	useLLthrowInPromise: useLLthrowInPromise,
 	useLLthrowImmediately: useLLthrowImmediately,
 	useLLdontThrowImmediately: useLLdontThrowImmediately,
 	useLLmayThrowImmediately: useLLmayThrowImmediately,
 	useLLmayRejectInPromise: useLLmayRejectInPromise,
 	mayThrowImmediately: mayThrowImmediately,
 	mayRejectInPromise: mayRejectInPromise,
 	fulfillInPromise: fulfillInPromise,
 	rejectInPromise: rejectInPromise,
 	throwInPromise: throwInPromise,
 	throwImmediately: throwImmediately,
 	dontThrowImmediately: dontThrowImmediately
 }
